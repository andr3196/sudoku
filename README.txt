Welcome to the Sudoku-based speed comparison of programming languages.
In this project we test the speed of different languages of solving sudokus.

---------------------
--- The Challenge ---
---------------------
- The files X.txt for X = 17,25, 30, 35, 40, 45 in folder "sudokus" contains 100  sudokus with X clues;
- For each of the prgramming languages: MATLAB, Python, C, C++, and  Java we consider:
	- The solving algorithm:
	    - Backtracking OOP
	    - Backtracking functional
	    - Candidate canceling (heuristic)

	- A runnable for each language will be produced. The runnable will read in the files one by one, construct a sudoku for each line, time solving, and report an average time for solving each combination of X and algorithm.

- We will save these results to a file and compare them.
 



import numpy as np
from core.heuristic import ApplyAll
from common.io.io import import_sudoku
from core.sudoku.sudoku import Sudoku
from core.heuristic import HeuristicSolver


""" This is a runner to show how this software is used.

"""

M_generator = import_sudoku('17.txt', '../../../Data/')

app_strat = ApplyAll()
for _ in np.arange(5):
    M = next(M_generator)
    S = Sudoku(M)
    solver = HeuristicSolver(S,
                             app_strat,
                             maxit=1000,
                             should_apply_backtracking=True,
                             backtracking_maxit=5000)
    solver.solve()
    print(solver.status)

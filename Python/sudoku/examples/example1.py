from core.sudoku.sudoku import Sudoku
from core.solvers import Solver
from common.io.io import import_sudoku

M = import_sudoku('25.txt', '../../../Data/')
print(M)
S = Sudoku(M)
sol = Solver(S, maxit=10000)
sol.solve()
sol.sudoku.print()
print(sol.status)
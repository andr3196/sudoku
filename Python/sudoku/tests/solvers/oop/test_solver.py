import unittest
from examples.example1 import import_sudoku
from core.sudoku.sudoku import Sudoku
from core.solvers import Solver
import numpy as np


class MySolverTests(unittest.TestCase):
    def setUp(self):
        M = import_sudoku('45.txt', '../../../Data/')
        M2 = np.array(
            [5, 6, 1, 8, 4, 7, 9, 2, 3, 3, 7, 9, 5, 2, 1, 6, 8, 4, 4, 2, 8, 9, 6, 3, 1, 7, 5, 6, 1, 3, 7, 8, 9, 5, 4, 2,
             7, 9, 4, 6, 5, 2, 3, 1, 8, 8, 5, 2, 1, 3, 4, 7, 9, 6, 9, 3, 5, 4, 7, 8, 2, 6, 1, 1, 4, 6, 2, 9, 5, 8, 3, 7,
             2, 8, 7, 3, 1, 6, 4, 5, 9]
            )
        M2.shape = (9,9)
        self.Sudoku = Sudoku(M)
        self.solvedSudoku = Sudoku(M2)
        self.Solver = Solver(self.Sudoku)

    def test_can_find_next(self):
        self.assertEqual(self.Solver.find_next_zero(0),0)
        self.assertEqual(self.Solver.find_next_zero(2),4)

    def test_can_increment_0_to_1_to_2_to_3(self):
        self.Solver.increment(0)
        self.assertEqual(self.Solver.sudoku[0], 1)
        self.Solver.increment(0)
        self.Solver.increment(0)
        self.assertEqual(self.Solver.sudoku[0], 3)


    def test_increments_9_to_0(self):
        self.Solver.increment(72)
        self.assertEqual(self.Solver.sudoku[72], 0)

    def test_appends_to_placed_list(self):
        self.Solver.increment(0)
        self.assertEqual(len(self.Solver.sudoku.P), 1)
        self.assertEqual(self.Solver.sudoku.P[0], 0)
        # Subsequent increments of the same field do not get appended
        self.Solver.increment(0)
        self.assertEqual(len(self.Solver.sudoku.P), 1)
        # but other fields do
        self.Solver.increment(1)
        self.assertEqual(len(self.Solver.sudoku.P), 2)





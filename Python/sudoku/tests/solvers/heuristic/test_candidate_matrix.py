
import numpy as np
from core.heuristic import CandidateMatrix
from common.io.io import import_sudoku
from core.sudoku.sudoku import Sudoku
from tests.solvers.heuristic.heuristic_base_class import HeuristicBaseTest


class CandidateMatrixTest(HeuristicBaseTest):

    # Test sudoku:
    #[[3 7 0 0 0 0 0 0 1]
    # [0 0 0 7 0 0 0 0 5]
    # [4 0 8 0 6 1 0 9 0]
    # [0 0 0 0 1 0 0 0 0]
    # [0 5 0 0 9 0 4 6 0]
    # [0 8 6 0 0 2 0 3 0]
    # [0 0 0 0 0 0 0 0 0]
    # [6 9 4 0 0 5 2 0 3]
    # [8 0 0 1 4 9 5 0 0]]

    def setUp(self):
        M_generator = import_sudoku('30.txt', '../../../Data/')
        self.S = Sudoku(next(M_generator))
        self.C = CandidateMatrix(self.S)

    def test_that_C_initialized_correctly(self):
        self.assertEqual(len(self.C[2]), 9)
        self.assertEqual(len(self.C[0]), 0)

    def test_that_C_can_get_item(self):
        # single  integer
        self.assertIsNotNone(self.C[56])
        # single slice
        self.assertIsNotNone(self.C[3:7])
        # integer and slice
        self.assertIsNotNone(self.C[3:6,2])
        self.assertIsNotNone(self.C[2,3:6])
        # slice and slice
        self.assertIsNotNone(self.C[4:8, 4:7])
        # slice with none
        self.assertIsNotNone(self.C[:5,0])
        # slice with none to none
        self.assertIsNotNone(self.C[0,:])

    def test_that_C_can_set_item(self):
        # Set single element
        self.C[0] = 4
        self.assertEqual(self.C[0], 4)
        # Set multiple elements (linear index)
        self.C[:3] = np.array([1, 2, 3])
        self.assertArrayIsEqual(self.C[0], [1, 2, 3])
        self.assertArrayIsEqual(self.C[1], [1,2,3])
        self.assertArrayIsEqual(self.C[2], [1,2, 3])
        # Test indedependence of entries
        self.C[1] *= 2
        self.assertArrayIsEqual(self.C[2],[1,2,3])
        # Set single element (tuple index)
        self.C[1,1] = np.array([1,2,3])
        self.assertArrayIsEqual(self.C[1,1], [1,2,3])
        # Set multiple elements (tuple and slice)
        self.C[:6,5] = np.array([1,2,3])
        for i in np.arange(0,6):
            self.assertArrayIsEqual(self.C[i,5], [1,2,3])
        # Set multiple elements (tuple of slices)
        self.C[6:9,7:9] = np.array([1,2,3,4,5])
        for row in np.arange(6,9):
            for col in np.arange(7,9):
                self.assertArrayIsEqual(self.C[row,col], [1,2,3,4,5])


    def test_row_exclude(self):
        row_indices = np.arange(9)
        self.C.exclude_values(row_indices, [1, 2, 3])
        for i in np.arange(9):
            self.assertArrayDoesNotContainValues(self.C[i], [1,2,3])

    def test_col_exclude(self):
        col_indices = np.arange(0,81,9)
        self.C.exclude_values(col_indices, [1, 2, 3])
        for i in np.arange(0,81,9):
            self.assertArrayDoesNotContainValues(self.C[i], [1,2,3])

    def test_square_exclude(self):
        square_indices = self.S.get_abs_block_indices(3, 3)
        self.C.exclude_values(square_indices, [4,5,6])
        for r in np.arange(3,6):
            for c in np.arange(3,6):
                self.assertArrayDoesNotContainValues(self.C[r,c], [4, 5,6 ])

    def test_tally_number_occurences(self):
        C_slice = [np.array([1, 2, 3]),
                   np.array([1, 2, 3]),
                   np.array([4, 5, 6, 7, 8]),
                   np.array([1]),
                   np.array([]),
                   np.array([1, 2]),
                   np.array([4, 5, 6]),
                   np.array([1, 2, 8]),
                   np.array([1, 2, 3, 4, 5])]
        tally = self.C.tally_number_occurrences(C_slice)
        self.assertEqual(tally[0][0], 6); self.assertEqual(tally[0][1], 8)
        self.assertEqual(tally[1][0], 5); self.assertEqual(tally[1][1], 8)
        self.assertEqual(tally[2][0], 3); self.assertEqual(tally[2][1], 8)
        self.assertEqual(tally[3][0], 3); self.assertEqual(tally[3][1], 8)
        self.assertEqual(tally[4][0], 3); self.assertEqual(tally[4][1], 8)
        self.assertEqual(tally[5][0], 2); self.assertEqual(tally[5][1], 6)
        self.assertEqual(tally[6][0], 1); self.assertEqual(tally[6][1], 2)
        self.assertEqual(tally[7][0], 2); self.assertEqual(tally[7][1], 7)
        self.assertEqual(tally[8][0], 0); self.assertEqual(tally[8][1], 0)


    def test_can_print_square_candidates(self):
        pass
        #self.candidates.print('squares')


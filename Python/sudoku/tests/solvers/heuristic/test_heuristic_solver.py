from common.iterator_type import IteratorType
from core.sudoku.sudoku import Sudoku
from common.io.io import import_sudoku
from core.heuristic import HeuristicSolver
from tests.solvers.heuristic.heuristic_base_class import HeuristicBaseTest
from core.heuristic import ApplyAll
import numpy as np
from core.heuristic.abstract import AbstractSquare as AS


class HeuristicSolverTest(HeuristicBaseTest):
    # Test sudoku:
    # [[3 7 0 0 0 0 0 0 1]
    # [0 0 0 7 0 0 0 0 5]
    # [4 0 8 0 6 1 0 9 0]
    # [0 0 0 0 1 0 0 0 0]
    # [0 5 0 0 9 0 4 6 0]
    # [0 8 6 0 0 2 0 3 0]
    # [0 0 0 0 0 0 0 0 0]
    # [6 9 4 0 0 5 2 0 3]
    # [8 0 0 1 4 9 5 0 0]]

    def setUp(self):
        M_generator = import_sudoku('30.txt', '../../../Data/')
        S = Sudoku(next(M_generator))
        app_strat = ApplyAll()
        self.solver = HeuristicSolver(S, app_strat)

    def test_if_Ci_has_1_candidate_Mi_is_assigned_this_candidate(self):
        self.solver.candidate_matrix[2] = [2]
        self.solver.fill_in_candidates()
        # After filling in candidates, the sudoku should have the appropriate value
        self.assertEqual(self.solver.sudoku[2], 2)
        # and the entry in the candidate matrix should be empty
        self.assertTrue(len(self.solver.candidate_matrix[2]) == 0)

    def test_apply_uniqueness_removes_candidates_in_row(self):
        # Initially 7 is a candidate in S[2]
        self.solver.apply_unique_value()
        self.assertArrayDoesNotContainValues(self.solver.candidate_matrix[2], [3, 7, 1])

    def test_apply_uniqueness_removes_candidates_in_column(self):
        # Initially [1 5 3] are candidates in column 8, so they should be eliminated from candidates[62]
        self.solver.apply_unique_value()
        self.assertArrayDoesNotContainValues(self.solver.candidate_matrix[62], [1, 5, 3])

    def test_apply_uniqueness_removes_candidates_in_square(self):
        # Initially [1 4 5 9] are in square with S[57], so they should be eliminated from candidates[57]
        self.solver.apply_unique_value()
        self.assertArrayDoesNotContainValues(self.solver.candidate_matrix[57], [1, 4, 5, 9])

    def test_can_apply_exclusive_value_row(self):
        # As the value 4 can only be placed at S[0], we expect the field candidates[0] to only contain 4 after applying
        # find_exclusive_value
        C_slice = [[1, 2, 3, 4], [1, 2, 3], [], [], [5, 6, 7, 8], [6, 7], [5, 7, 8], [2, 3], [1]]
        self.solver.candidate_matrix[:9] = C_slice
        self.solver._apply_exclusive_values(np.arange(9), C_slice)
        self.assertArrayIsEqual(self.solver.candidate_matrix[0], [4])

    def test_can_apply_exclusive_value_col(self):
        # As the value 8 can only be place at S[54], we expect the field candidates[54]to only contain 8 after applying
        # find_exclusive_value
        C_slice = [[1, 2, 3, 4], [1, 2, 3, 4], [], [], [5, 6, 7], [6, 7], [5, 7, 8], [2, 3], [1]]
        self.solver.candidate_matrix[:, 0] = C_slice
        self.solver._apply_exclusive_values(np.arange(0, 81, 9), C_slice)
        self.assertArrayIsEqual(self.solver.candidate_matrix[54], [8])

    def test_can_apply_exclusive_value_square(self):
        C_slice = [[1, 2, 3, 4], [1, 2, 3, 4], [], [], [5, 6, 7, 9], [6, 7], [5, 7, 8], [2, 3, 8], [1]]
        self.solver.candidate_matrix[3:6, 3:6] = C_slice
        self.solver._apply_exclusive_values(np.concatenate((np.arange(30, 33), np.arange(39, 42), np.arange(48, 51))),
                                            C_slice)
        self.assertArrayIsEqual(self.solver.candidate_matrix[40], [9])

    def test_can_apply_exclusive_value(self):
        # test the main method for this heuristic
        self.solver.apply_unique_value()
        self.solver.apply_exclusive_value()
        self.assertArrayIsEqual(self.solver.candidate_matrix[6], [6])

    ##### Test __get_block_index____ ########

    def test_returns_indim012_for_get_block_indices_dim0_row0(self):
        # Should get the indices of the first row of a block indim = [0 1 2] and rest = [ 3 4 5 6 7 8]

        indim, rest = AS.get_rel_indim_block_indices(0, 0)
        self.assertArrayIsEqual(indim, [0, 1, 2])

    def test_returns_rest3to8_for_get_block_indices_dim0_row0(self):
        indim, rest = AS.get_rel_indim_block_indices(0, 0)
        self.assertArrayIsEqual(rest, np.arange(3, 9))

    def test_returns_indim345_for_get_block_indices_dim0_row0(self):
        indim, rest = AS.get_rel_indim_block_indices(0, 1)
        self.assertArrayIsEqual(indim, [3, 4, 5])

    def test_returns_rest012678_for_get_block_indices_dim0_row0(self):
        indim, rest = AS.get_rel_indim_block_indices(0, 1)
        self.assertArrayIsEqual(rest, [0, 1, 2, 6, 7, 8])

    def test_returns_indim036_for_get_block_indices_dim1_col0(self):
        indim, rest = AS.get_rel_indim_block_indices(1, 0)
        self.assertArrayIsEqual(indim, [0, 3, 6])

    def test_returns_indim258_for_get_block_indices_dim1_col2(self):
        indim, rest = AS.get_rel_indim_block_indices(1, 2)
        self.assertArrayIsEqual(indim, [2, 5, 8])

    def test_returns_rest013467_for_get_block_indices_dim1_col2(self):
        indim, rest = AS.get_rel_indim_block_indices(1, 2)
        self.assertArrayIsEqual(rest, [0, 1, 3, 4, 6, 7])

    ####### End test __get_block_index #########
    ##### Begin test _apply_block_dim_interaction ##########
    def prepare_for_test_group(self):
        self.solver.apply_unique_value()

    def test_rel_to_abs_indices_returns_3to8_for_indim012_in_block00_i0(self):
        # dim = 0, i = 0, b1 = 0, b2 = 0
        indices = AS.rel_to_abs_indices(0, 0, 0, 0, np.arange(3, 9))
        self.assertArrayIsEqual(indices, np.arange(3, 9))

    def test_rel_to_abs_indices_returns_27_36_45_54_63_72_for_indim036_in_block00_i0(self):
        # dim = 1, i = 0, b1 = 0, b2 = 0
        indices = AS.rel_to_abs_indices(1, 0, 0, 0, np.arange(3, 9))
        self.assertArrayIsEqual(indices, np.arange(27, 81, 9))

    def test_rel_to_abs_indices_returns_27_36_45_54_63_72_for_indim036_in_block00_i1(self):
        # dim = 1, i = 0, b1 = 0, b2 = 0
        indices = AS.rel_to_abs_indices(1, 1, 0, 0, np.arange(3, 9))
        self.assertArrayIsEqual(indices, np.arange(28, 81, 9))

    def test_rel_to_abs_indices_returns_27_36_45_54_63_72_for_indim036_in_block33_i0(self):
        # dim = 1, i = 0, b1 = 0, b2 = 0
        indices = AS.rel_to_abs_indices(1, 0, 3, 3, np.array([0, 1, 2, 6, 7, 8]))
        self.assertArrayIsEqual(indices, [3, 12, 21, 57, 66, 75])

    def test_can_apply_block_dim_interaction(self):
        #
        self.prepare_for_test_group()
        self.solver.apply_block_dim_interaction()
        self.assertArrayDoesNotContainValues(self.solver.candidate_matrix[6, 6], [6])
        self.assertArrayDoesNotContainValues(self.solver.candidate_matrix[0, 3], [5])

    ##### End test _apply_block_dim_interaction ###########

    #### Test naked_subset ####
    #### Test _find_identical_candidate_tuples ####
    def test_find_pair47_in_row(self):
        C_slice = [[4, 7], [1, 4, 7], [], [2, 4, 6, 7], [4, 7], [2, 4, 7], [], [1, 2, 3, 4, 6, 7], []]
        pairs, indices = self.solver._find_identical_candidate_tuples(C_slice)
        self.assertArrayIsEqual(pairs[0], [4, 7])
        self.assertArrayIsEqual(indices[0], [0, 4])

    def test_find_triple123_and_pair47_in_row(self):
        C_slice = [[4, 7], [1, 2, 3], [], [2, 4, 6, 7], [4, 7], [2, 4, 7], [1, 2, 3], [1, 2, 3, 4, 6, 7], [1, 2, 3]]
        pairs, indices = self.solver._find_identical_candidate_tuples(C_slice)
        self.assertArrayIsEqual(pairs[0], [4, 7])
        self.assertArrayIsEqual(indices[0], [0, 4])
        self.assertArrayIsEqual(pairs[1], [1, 2, 3])
        self.assertArrayIsEqual(indices[1], [1, 6, 8])

    def test_find_no_pairs(self):
        C_slice = [[4, 7], [1, 2, 3], [], [], [4, 7, 5], [2, 4, 7], [1, 2, 3, 4], [1, 2, 3, 4, 6, 7], [2, 3]]
        pairs, indices = self.solver._find_identical_candidate_tuples(C_slice)
        self.assertEqual(len(pairs), 0)
        self.assertEqual(len(indices), 0)

    def test_find_no_pairs2(self):
        C_slice = [[], [1, 2, 3], [], [2, 4, 6, 7], [4, 7], [2, 4, 7], [1, 2, 3], [1, 2, 3, 4, 6, 7], []]
        pairs, indices = self.solver._find_identical_candidate_tuples(C_slice)
        self.assertEqual(len(pairs), 0)
        self.assertEqual(len(indices), 0)
        #### End _find_identical_candidate_tuples ####

    def prepare_for_non_pairs_1(self):
        l = [[] for _ in self.solver.CONST_0to80]
        l[0] = [4, 7]
        l[9] = [1, 4, 7]
        l[27] = [2,4,6,7]
        l[36] = [4,7]
        l[45] = [2,4,7]
        l[63] = [1,2,3,4,6,7]

        self.solver.candidate_matrix.candidates = l

        #### Test __eliminate_non_pairs ####

    def test_can_eliminate_non_pairs1(self):
        self.prepare_for_non_pairs_1()
        self.solver.candidate_matrix.set_iterator(IteratorType.COL_ITER)
        for c_slice, abs_indices in self.solver.candidate_matrix:
            pairs, indices = self.solver._find_identical_candidate_tuples(c_slice)
            self.solver._eliminate_non_pairs(pairs, indices, abs_indices)
        self.assertArrayDoesNotContainValues(self.solver.candidate_matrix[9], [4,7])
        self.assertArrayDoesNotContainValues(self.solver.candidate_matrix[27], [4, 7])
        self.assertArrayDoesNotContainValues(self.solver.candidate_matrix[45], [4, 7])
        self.assertArrayDoesNotContainValues(self.solver.candidate_matrix[63], [4, 7])


    def test_can_apply_non_pairs2(self):

        pass

        #### End __eliminate_non_pairs ####

    #### End naked_subset ####

    ### Test apply_backtracking ###
    def test_apply_backtracking(self):
        self.prepare_for_test_group()
        self.solver.apply_exclusive_value()
        self.test_can_apply_block_dim_interaction()
        self.solver.fill_in_candidates()
        self.solver.apply_backtracking()
        self.assertTrue(self.solver.sudoku.is_valid())

    ### End apply_backtracking ###

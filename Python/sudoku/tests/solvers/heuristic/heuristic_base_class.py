import unittest
from common import *


class HeuristicBaseTest(unittest.TestCase):
    def assertArrayIsEqual(self, arr_1: ListLike, arr_2: ListLike) -> None:
        """ Fail if the two arrays are not equal

        Args:
            :param arr_1: An array of numbers
            :param arr_2: An array of numbers

        Raises:
            AssertionException - the two are not equal
        """
        if len(arr_1) != len(arr_2):
            self.fail(
                "The lengths of the two arrays should be equal, but were " + str(len(arr_1)) + " and " + str(
                    len(arr_2)))
        for i in np.arange(len(arr_1)):
            if arr_1[i] != arr_2[i]:
                self.fail(
                    "Two elements of the arrays do not match: arr_1[" + str(i) + "] = " + str(
                        arr_1[i]) + ", but arr_2[" + str(
                        i) + "] = " + str(arr_2[i]))

    def assertArrayDoesNotContainValues(self, arr: ListLike, values: ListLike) -> None:
        """Fail if any value value in values is in arr

        Args:
            :param arr: An array of numbers
            :param values: An array of numbers

        Raises:
            AssertionException - the array does contain an unwanted value
        """
        for value in values:
            if value in arr:
                self.fail("Expected array to not contain value, but " + str(arr) + " contains " + str(value))

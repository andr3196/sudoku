from core.heuristic import ApplyAll
from tests.solvers.heuristic.heuristic_base_class import HeuristicBaseTest
from core.heuristic import HeuristicSolver
from core.heuristic.abstract import AbstractSquare
from core.sudoku.sudoku import Sudoku
from common.io.io import import_sudoku


class AbstractSquareTest(HeuristicBaseTest):
    # Test sudoku:
    #[[3 7 0 0 0 0 0 0 1]
    # [0 0 0 7 0 0 0 0 5]
    # [4 0 8 0 6 1 0 9 0]
    # [0 0 0 0 1 0 0 0 0]
    # [0 5 0 0 9 0 4 6 0]
    # [0 8 6 0 0 2 0 3 0]
    # [0 0 0 0 0 0 0 0 0]
    # [6 9 4 0 0 5 2 0 3]
    # [8 0 0 1 4 9 5 0 0]]

    def setUp(self):
        M_generator = import_sudoku('30.txt', '../../../Data/')
        S = Sudoku(next(M_generator))
        app_strat = ApplyAll()
        self.solver = HeuristicSolver(S, app_strat)

    def test_get_colrow_indices_from_abs_square_indices(self):
        # test 1
        dim = 0
        i = 0
        indices = [0,1,2, 9,10,11, 18, 19,20]
        in_index, out_index = AbstractSquare.get_colrow_indices_from_abs_square_indices(dim, i, indices)
        self.assertArrayIsEqual(in_index, [0,1,2])
        self.assertArrayIsEqual(out_index, [3,4,5,6,7,8])
        # test 2
        dim = 0
        i = 1
        indices = [30, 31, 32, 39, 40, 41, 48, 49, 50]
        in_index, out_index = AbstractSquare.get_colrow_indices_from_abs_square_indices(dim, i, indices)
        self.assertArrayIsEqual(in_index, [39, 40, 41])
        self.assertArrayIsEqual(out_index, [36, 37, 38, 42, 43, 44])
        # test 3
        dim = 0
        i = 2
        indices = [60, 61, 62, 69, 70, 71, 78, 79, 80]
        in_index, out_index = AbstractSquare.get_colrow_indices_from_abs_square_indices(dim, i, indices)
        self.assertArrayIsEqual(in_index, [78, 79, 80])
        self.assertArrayIsEqual(out_index, [72, 73, 74, 75, 76, 77])
        # test 4
        dim = 1
        i = 2
        indices = [60, 61, 62, 69, 70, 71, 78, 79, 80]
        in_index, out_index = AbstractSquare.get_colrow_indices_from_abs_square_indices(dim, i, indices)
        self.assertArrayIsEqual(in_index, [62, 71, 80])
        self.assertArrayIsEqual(out_index, [8, 17, 26, 35, 44, 53])
        #test 5
        dim = 1
        i = 0
        indices = [0, 1, 2, 9, 10, 11, 18, 19, 20]
        in_index, out_index = AbstractSquare.get_colrow_indices_from_abs_square_indices(dim, i, indices)
        self.assertArrayIsEqual(in_index, [0, 9, 18])
        self.assertArrayIsEqual(out_index, [27, 36, 45, 54, 63, 72])


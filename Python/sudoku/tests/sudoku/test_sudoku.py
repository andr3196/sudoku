import unittest
from examples.example1 import import_sudoku
from core.sudoku.sudoku import Sudoku
import numpy as np


class MySudokuTests(unittest.TestCase):
    def setUp(self):
        M = import_sudoku('45.txt', '../../../Data/')
        M2 = np.array(
            [5, 6, 1, 8, 4, 7, 9, 2, 3, 3, 7, 9, 5, 2, 1, 6, 8, 4, 4, 2, 8, 9, 6, 3, 1, 7, 5, 6, 1, 3, 7, 8, 9, 5, 4, 2,
             7, 9, 4, 6, 5, 2, 3, 1, 8, 8, 5, 2, 1, 3, 4, 7, 9, 6, 9, 3, 5, 4, 7, 8, 2, 6, 1, 1, 4, 6, 2, 9, 5, 8, 3, 7,
             2, 8, 7, 3, 1, 6, 4, 5, 9]
            )
        M2.shape = (9,9)
        self.Sudoku = Sudoku(M)
        self.solvedSudoku = Sudoku(M2)

    def test_can_use_indexing(self):
        # get first row
        true_result = np.array([0, 0, 1, 7, 0, 0, 5, 0, 9])
        comp_array = np.equal(self.Sudoku[0,:], true_result)
        self.assertTrue(np.all(comp_array))
        # get first column
        true_result = np.array([0, 5, 8, 7, 0, 6, 4, 0, 9])
        comp_array = np.equal(self.Sudoku[:, 0], true_result)
        self.assertTrue(np.all(comp_array))

    def test_has_duplicates(self):

        self.assertTrue(self.Sudoku.has_duplicates([5, 0, 1, 2, 3, 4, 5]))
        self.assertFalse(self.Sudoku.has_duplicates([0, 1, 2, 3, 4, 5]))

    def test_that_row_with_zeros_is_not_a_valid_row(self):
        self.assertFalse(self.Sudoku.check_row(0))

    def test_that_row_with_repeated_numbers_is_not_valid(self):
        bad_row = np.arange(1, 10)
        bad_row[4] = 2
        self.Sudoku[0,:] = bad_row

        self.assertFalse(self.Sudoku.check_row(0))

    def test_that_row_of_1to9_is_valid(self):
        self.assertTrue(self.solvedSudoku.check_row(0))

    def test_that_row_of012345678_is_not_valid(self):
        self.Sudoku[0,:] = np.arange(9)
        self.assertFalse(self.Sudoku.check_row(0))

    def test_check_column(self):
        # Should be false if the sudoku contains zeros
        self.assertFalse(self.Sudoku.check_col(0))
        # Should be true if the sudoku is solved
        self.assertTrue(self.solvedSudoku.check_col(0))
        # Should be false if the sudoku has repeated values
        bad_col = np.arange(1, 10)
        bad_col[4] = 2
        self.Sudoku[:,3] = bad_col
        self.assertFalse(self.Sudoku.check_col(3))

    def test_check_square(self):
        # Should be false if the sudoku square contains zeros
        self.assertFalse(self.Sudoku.check_square(2,1))
        # Should be true if the sudoku is solved
        self.assertTrue(self.solvedSudoku.check_square(2,1))
        # Should be false if the sudoku has repeated values
        self.solvedSudoku[0,0] = 4
        self.assertFalse(self.Sudoku.check_square(0,0))

    def test_is_valid(self):
        # if it has zeros, it cannot be valid
        self.assertFalse(self.Sudoku.is_valid())
        # if is a solved sudoku, it must be valid
        self.assertTrue(self.solvedSudoku.is_valid())
        # if it has repeated values it must be false
        self.solvedSudoku[78:80] = 2
        self.assertFalse(self.solvedSudoku.is_valid())

    def test_is_violating(self):
        # Initially each row, col and block must not be violating
        self.assertTrue(self.Sudoku.check_row(0, ignore_0=True))
        self.assertTrue(self.Sudoku.check_col(3, ignore_0=True))
        self.assertTrue(self.Sudoku.check_square(7,7,ignore_0=True))
        self.Sudoku[78] = 7
        self.assertFalse(self.Sudoku.check_row(8, ignore_0=True))
        self.Sudoku[0,5] = 4
        self.assertFalse(self.Sudoku.check_col(5, ignore_0=True))
        self.assertFalse(self.Sudoku.check_square(8,8,ignore_0=True))

        self.Sudoku.print()
        self.assertFalse(self.Sudoku.is_violating(9))
        self.assertTrue(self.Sudoku.is_violating(5)) # violates col
        self.assertTrue(self.Sudoku.is_violating(77)) # violates row
        self.assertTrue(self.Sudoku.is_violating(61)) # violates block






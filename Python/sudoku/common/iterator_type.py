from enum import Enum


class IteratorType(Enum):
    ROW_ITER = 0
    COL_ITER = 1
    BLOCK_ITER = 2
    ALL_ITER = 3
from typing import Union, List, Callable, Tuple, Generator
import numpy as np
IntLike = Union[int, slice]
ListLike = Union[List, np.ndarray]
ListOfLists = List[List]
Iterator = Union['RowIterator', 'ColIterator', 'BlockIterator', 'AllIterator']
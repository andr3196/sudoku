import numpy as np
from core.heuristic import StrategyBuilder
from core.sudoku.sudoku import Sudoku
from core.heuristic import HeuristicSolver
import sys
import time

""" Interface to prgrammatically interact with the sudoku solver via the command line"""

# Get string of number the characterises the sudoku

sudoku_str = sys.argv[1]  # ["sudoku_control.py", "05007060030..."]
print(sudoku_str)

M = np.zeros((9, 9), dtype=np.int8)
for i in range(81):
    M.flat[i] = int(sudoku_str[i])

S = Sudoku(M)

print("Waiting for init")


with open("../../sudoku/Python/sudoku/pipe", "r+") as pipe:
    params = pipe.readline()
    pipe.truncate(0)


print("Python says: " + params)
builder = StrategyBuilder()


builder.should_apply_unique_value = "UNIQUE_VALUE" in params
builder.should_apply_exclusive_value = "EXCLUSIVE_VALUE" in params
builder.should_apply_block_dim_interaction = "BLOCK_DIM_INTERACTION" in params
should_apply_backtracking = "BACKTRACKING" in params

solver = HeuristicSolver(S,
                         builder.build(),
                         maxit=1000,
                         should_apply_backtracking=should_apply_backtracking,
                         backtracking_maxit=5000)
print("Waiting for start")

should_start = ""

with open("../../sudoku/Python/sudoku/pipe", "r") as pipe:
    while True:
        print("Reading")
        should_start = pipe.readline()
        if should_start:
            print("Should start: " + should_start)
            break
        time.sleep(1)

print("Start received - solving")
solver.solve()
print("Done!")


print(solver.status)
print(str(S))


from common import *


def import_sudoku(filename: str, dir: str=None) -> Generator:
    """ import a sudoku from a text file which has sudokus listed as 81 numbers per line. Missing values are noted
     with "0"

    :param filename: The name of the sudoku file.
    :param dir: The relative path between the current working directive and the directive containing the sudoku file.
    :return: A generator of sudokus.
    """

    if dir is None:
        name = filename
    else:
        name = dir + filename
    f = open(name, 'r')
    M = np.zeros((9, 9), dtype=np.int8)
    for line in f:
        for i in range(81):
            M.flat[i] = int(line[i])
        yield M
    f.close()

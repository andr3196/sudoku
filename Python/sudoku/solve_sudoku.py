import sys

# called as python3 "BACKTRACKINGUN.." "0796805.."
import numpy as np

from core.sudoku.sudoku import Sudoku
from core.heuristic import StrategyBuilder
from core.heuristic import HeuristicSolver


def solve_sudoku(argv):
    heuristics_str = argv[1]
    sudoku_str = argv[2]
    M = np.zeros((9, 9), dtype=np.int8)
    for i in range(81):
        M.flat[i] = int(sudoku_str[i])

    S = Sudoku(M)
    builder = StrategyBuilder()

    builder.should_apply_unique_value = "UNIQUE_VALUE" in heuristics_str
    builder.should_apply_exclusive_value = "EXCLUSIVE_VALUE" in heuristics_str
    builder.should_apply_block_dim_interaction = "BLOCK_DIM_INTERACTION" in heuristics_str
    should_apply_backtracking = "BACKTRACKING" in heuristics_str

    solver = HeuristicSolver(S,
                             builder.build(),
                             maxit=1000,
                             should_apply_backtracking=should_apply_backtracking,
                             backtracking_maxit=5000)
    solver.solve()


if __name__ == "__main__":
    solve_sudoku(sys.argv)

import numpy as np

from common.iterator_type import IteratorType
from core.heuristic.abstract import AbstractSquare


class Sudoku(AbstractSquare):

    def __init__(self, M):
        super().__init__()
        if not M.shape == (9,9):
            raise ValueError("A sudoku must be 9x9, but was: " + str(M.shape))
        self.M = M
        self.sz = np.size(M)
        self.P = []  # list of placed elements
        self.num_filled = np.count_nonzero(M)
        self.cur_iter_mode = IteratorType.ALL_ITER

    def check_row(self, row, ignore_0 = False) -> bool:
        row = self.M[row,:]
        if ignore_0:
            row[row == 0] = False
            return not self.has_duplicates(row)
        return self.is_permutation_of_1to9(row)

    def check_col(self, col, ignore_0 = False) -> bool:
        col = self.M[:,col]
        if ignore_0:
            col[col == 0] = False
            return not self.has_duplicates(col)
        return self.is_permutation_of_1to9(col)

    def check_square(self, row, col, ignore_0 = False) -> bool:
        rb, re, cb, ce = self.get_containing_block_index(row,col)
        block = self.M[rb:re,cb:ce]
        if ignore_0:
            block[block == 0] = False
            return not self.has_duplicates(block)
        return self.is_permutation_of_1to9(block)

    def has_empty_placedList(self) -> bool:
        return len(self.P) == 0

    def append_value_to_placedList(self,v):
        self.P.append(v)

    def pop_placedList(self) -> int:
        return self.P.pop()

    def is_valid(self, i = None):
        if i is not None:
            sq_rows = rows = [np.floor(i/9)]
            sq_cols = cols = [i % 9]
        else:
            rows = cols = np.arange(9)
            sq_rows = sq_cols = np.arange(0,9,3)

        for row in rows:
            if not self.check_row(row):
                return False
        for col in cols:
            if not self.check_col(col):
                return False
        for row in  sq_rows:
            for col in sq_cols:
                if not self.check_square(row,col):
                    return False
        return True

    def is_violating(self, i):
        row, col = self.linind_to_row_col(i)
        return not (self.check_row(row, ignore_0=True) and self.check_col(col, ignore_0=True) and self.check_square(row,col,ignore_0=True))

    # Assumes that there is at least 1 element in P
    def peak_placedList(self) -> int:
        return self.P[-1]

    def print(self):
        print(self.M)

    def __getitem__(self, item) -> int:

        if isinstance(item, tuple):
            return self.M[item]
        else:
            return self.M.flat[item]


    def __setitem__(self, key, value):
        if isinstance(key, tuple):
            self.M[key] = value
        else:
            self.M.flat[key] = value

    def __str__(self):
        s = ""
        for i in range(81):
            s += str(self.M.flat[i])
        return s


from core.sudoku.sudoku import Sudoku


class Solver:
    def __init__(self, sudoku: Sudoku, maxit=10000):
        self.sudoku = sudoku
        self.maxit = maxit
        self.status = "Initiated"

    def solve(self):
        i = 0
        it = 0
        while it < self.maxit:
            it += 1
            i = self.find_next_zero(i)
            if i >= self.sudoku.sz:
                break
            current_position_resolved = False
            while not current_position_resolved:
                did_increment = self.increment(i)
                if not did_increment:
                    self.sudoku.num_filled -= 1
                    self.sudoku.pop_placedList()
                    i = self.sudoku.peak_placedList()
                elif not self.sudoku.is_violating(i):
                    self.sudoku.num_filled += 1
                    current_position_resolved = True
        if it == self.maxit:
            self.status = "Unsolved, maxit hit"
        else:
            self.status = "Solved"

    def increment(self, i: int):
        if self.sudoku[i] == 9:
            self.sudoku[i] = 0
            return False
        self.sudoku[i] += 1
        if self.sudoku.has_empty_placedList() or i != self.sudoku.peak_placedList():
            self.sudoku.append_value_to_placedList(i)
        return True

    def find_next_zero(self, i: int):
        while i < self.sudoku.sz and self.sudoku[i] != 0:
            i += 1
        return i

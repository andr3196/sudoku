from common.io.io import import_sudoku
from typing import Tuple
import numpy as np

M = import_sudoku('30.txt', '../../../Data/')
maxit = 10000
sz = 81


def linind_to_row_col(linind):
    row = int(np.floor(linind / 9))
    col = linind % 9
    return row, col


def is_permutation_of_1to9(arr) -> bool:
    b = np.arange(1, 10)
    b[np.abs(arr - 1)] = 0
    return not np.any(b)


def get_containing_block_index(row, col) -> Tuple[int, int, int, int]:
    ri = int(np.ceil((row - 2) / 3.0))
    ci = int(np.ceil((col - 2) / 3.0))
    rb = 3 * ri
    re = rb + 2
    cb = 3 * ci
    ce = cb + 2
    return rb, re + 1, cb, ce + 1


def has_duplicates(arr):
    s = np.sort(arr, axis=None)
    return np.any(s[:-1][s[1:] == s[:-1]])


def check_row(M, row, ignore_0=False) -> bool:
    row = M[row, :]
    if ignore_0:
        row[row == 0] = False
        return not has_duplicates(row)
    return is_permutation_of_1to9(row)


def check_col(M, col, ignore_0=False) -> bool:
    col = M[:, col]
    if ignore_0:
        col[col == 0] = False
        return not has_duplicates(col)
    return is_permutation_of_1to9(col)


def check_square(M, row, col, ignore_0=False) -> bool:
    rb, re, cb, ce = get_containing_block_index(row, col)
    block = M[rb:re, cb:ce]
    if ignore_0:
        block[block == 0] = False
        return not has_duplicates(block)
    return is_permutation_of_1to9(block)


def is_violating(M, i):
    row, col = linind_to_row_col(i)
    return not (
        check_row(M, row, ignore_0=True) and check_col(M, col, ignore_0=True) and check_square(M, row, col,
                                                                                               ignore_0=True))


def increment(M, P, i: int):
    if M.flat[i] == 9:
        M.flat[i] = 0
        return False
    M.flat[i] += 1
    if not P or i != P[-1]:
        P.append(i)
    return True


def find_next_zero(M, i: int):
    while i < sz and M.flat[i] != 0:
        i += 1
    return i


def solve(M):
    i = 0
    it = 0
    P = []
    while it < maxit:
        it += 1
        i = find_next_zero(M, i)
        if i >= sz:
            break
        current_position_resolved = False
        while not current_position_resolved:
            did_increment = increment(M, P, i)
            if not did_increment:
                P.pop()
                i = P[-1]
            elif not is_violating(M, i):
                current_position_resolved = True
    print(M)
    if it == maxit:
        print("Unsolved, maxit hit")
    else:
        print("Solved")


solve(M)
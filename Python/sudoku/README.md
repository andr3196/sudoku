# Sudoku

A framework for solving sudoku puzzles

## Getting Started

To run the software, simply clone this repository and run with Python 3.

### Prerequisites

To run this software [numpy](http://www.numpy.org/) must be installed.

### Running

The software represents a sudoku as a numpy 9x9 matrix of integers with 0 where there is no value

Such a matrix can be constructed from a file using the `common.io.io.import_sudoku` or from the command line using `solve_sudoku`.

Such a matrix, M, is wrapped in a sudoku class

```
sudoku = Sudoku(M)
```

A solver can then be constructed from the Sudoku

```
solver = Solver(sudoku)
```

The sudoku can then be solved using `solver.solve()`. The status of the solver can be read from the `solver.status` attribute.


## Built With

* [PyCharm](https://www.jetbrains.com/pycharm/) - The IDE used

## Authors

* **Andreas Springborg**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

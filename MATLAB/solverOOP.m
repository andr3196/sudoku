function solverOOP(varargin)
% Solve Sudoku using an object oriented approach
% Depends on sudoku.m

if nargin > 0    
    S = sudoku(varargin{1});
else
    S = sudoku();
end

i = 1;


j = 0;

while i <= S.sz
    S.M
    j = j + 1;
    i = findNext0(S,i);
    if i >= S.sz 
        break
    end
    currentPositionUnresolved = false;
    while ~ currentPositionUnresolved
        [didIncrement, S] = increment(S,i);
        if ~ didIncrement
            i = S.popPlacedList();
        elseif isValidSudoku(S,i)
            currentPositionUnresolved = true;
        end 
    end
end
j
printSudoku(S)
end

function [didIncrement, S] = increment(S,i)
    if S(i) == 9
        S(i) = 0; 
        didIncrement = false;
        return
    else
        S(i) = S(i) + 1;
    end
    didIncrement = true;
    if S.hasEmptyPlacedList() || i ~= S.peakPlacedList()
        S.appendPlacedList(i);
    end
end 

function i = findNext0(S, i)
    while i <= S.sz && S(i) ~= 0
        i = i + 1;
    end
end

function r = isValidSudoku(S, i)
    col = ceil(i/9);
    row = mod(i-1,9)+1;
    r = S.checkCol(col) && S.checkRow(row) && S.checkSquare(row,col);
end
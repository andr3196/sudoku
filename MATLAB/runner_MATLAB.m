function runner_MATLAB()

nClues = [17, 25, 30, 35, 40, 45];
dataPath = '../Data/';
NSamples = 1;
times = zeros(size(nClues));

for i = 2:length(nClues)
   file = [num2str(nClues(i)) '.txt'];
   fid = fopen([dataPath file]);
   for j = 1:NSamples
        line = fgetl(fid);
        if isnumeric(line)
            break
        end
        C = textscan(line,'%1d');
        M = reshape(C{1},9,9).';
        f =@() solverFunc(M);
        times(i) = timeit(f);
   end
    
end

plot(nClues, times)
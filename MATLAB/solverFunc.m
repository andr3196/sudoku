function M = solverFunc(M)
N = numel(M);
P = [];
i = 1;
while i <= N
    % find next empty field
    while i < N && M(i) ~= 0
        i = i + 1;
    end
    if i == N 
        break
    end
    currentPositionUnresolved = false;
    while ~ currentPositionUnresolved
        [didIncrement, M(i), P] = increment(i, M(i), P);
        if ~ didIncrement
            i = P(end-1);
            P(end) = [];
        elseif isValidSudoku(M,i)
            currentPositionUnresolved = true;
        end 
    end
end


function [didIncrement, v, P] = increment(i, v, P)
    if v == 9
        v = 0; 
        didIncrement = false;
        return
    else
        v = v + 1;
    end
    didIncrement = true;
    if isempty(P) || i ~= P(end)
        P(end + 1) = i; 
    end
    
function r = isValidSudoku(M, i)
    col = ceil(i/9);
    row = mod(i-1,9)+1;
    % Check col
    C = M(:,col);
    C(C==0) = [];
    r = numel(C) == numel(unique(C));
    if ~r
        return
    end
    % Check row
    R = M(row,:);
    R(R==0) = [];
    r = numel(R) == numel(unique(R));
    if ~ r
        return
    end
    % Check square
    ri = ceil(row/3);
    ci = ceil(col/3);
    rb = (ri - 1)*3+1;
    re = 3*ri;
    cb = (ci - 1)*3+1;
    ce = 3*ci;
    S = M(rb:re,cb:ce);
    S(S==0) = [];
    r = numel(S) == numel(unique(S));
    
    
    
    
    
    
    
    
    
    

function solverHeuristic(M)

%%%% NB
% Silence warning with: warning('off', 'MATLAB:catenate:DimensionMismatch')
%%% 

C = initializeCandidates(M);

it = 0;
itMax = 200;
N = 81 - sum(sum(M == 0));
terminateflag = 0;
while ~ isFinishedSudoku(M)
    it = it +1;
    C = applyHeuristics(C,M);
    [C,M] = fillInCandidates(C,M);
    N1 = 81 - sum(sum(M == 0));
    dN = N - N1;
    N = N1;
    if dN < 1
        terminateflag = terminateflag - 1;
    end
    if terminateflag < 0
        disp('Termination flag activated')
        break
    end
    
end
M



function C = initializeCandidates(M)
C = repmat({1:9},1,81);
C(M ~= 0) = {[]};

function [C,M] = fillInCandidates(C,M)
for i = 1:81
    v = C{i};
   if length(v) == 1
       M(i) = v(1);
       C{i} = [];
   end
end

function C = applyHeuristics(C,M)
   % 1. If a value already exists in a given row/column/block then it
   % remove it as candidate from the remaining places
   C = uniquenessRemove(C,M);
   % 2. If a value can only be placed in one place in a row/column/block
   %C = exclusiveValue(C);
   % 3. If a value can only be placed in a certain row in a given block the
   % value can be eliminate from the rest of that row
   %C = blockDimInteraction(C);
   % 4. Block/block interaction: 
   C = blockBlockInteraction(C);

function C = uniquenessRemove(C,M)

% All rows must be unique
for row = 1:9
    R = M(row,:);
    for i = row:9:81
        if ~isempty(C{i})
        C{i} = setdiff(C{i},R);
        end
    end
end
for col = 1:9
    Col = M(:,col);
    for i = (col-1)*9 + 1:col*9
        if ~isempty(C{i})
        C{i} = setdiff(C{i},Col);
        end
    end
end
for block1 = 1:3:9
    e1 = block1 + 2;
    for block2 = 1:3:9
        e2 = block2 + 2;
        V = M(block1:e1,block2:e2);
        for i = block1:block1+2
            for j = block2:block2+2
                if ~isempty(C{i + 9*(j-1)})
                C{i + 9*(j-1)} = setdiff(C{i + 9*(j-1)}, V);
                end
            end
        end
        
    end
end

function Obs = countCandidateOccurences(C)
% Assumption C has 9 entries
Obs = zeros(9,2); % first column: number of occurances, second: index of last where
for i = 1:9
    Ccell = C{i};
    for c = 1:length(Ccell)
        Obs(Ccell(c),:) = [Obs(Ccell(c),1)+1, i];
    end
    
end

function C = applyExclusiveValue(C, Obs, i1, indexFunc,indexList)
ind = 1:9;
ind = ind(Obs(:,1) == 1);
for j = 1:length(ind) % values that are unique
    % In the cell very the exclusive candidate is we can eliminate other candidates
    % The cell index where the unique value is
    i2 = Obs(ind(j),2);
    %
    cind = indexList(i2);
    C{cind} = ind(j);
end

function C = exclusiveValue(C)
indexFunc = @(row, col) row + 9*(col - 1);
for row = 1:9
    % Introduce this of observed candidates
    % Run through all rows
     % - In each row count how many time a candidate exist 
     % - If it exist only once, we have an exclusive Value
    Obs = countCandidateOccurences(C(row:9:81));
    C = applyExclusiveValue(C, Obs, row,indexFunc,row:9:81);
end
% We switch row <-> col, as we are now iterating over columns rather than
% rows
indexFunc = @(col, row) indexFunc(row, col);
for col = 1:9
    Obs = countCandidateOccurences(C((col-1)*9 + 1:col*9));
    C = applyExclusiveValue(C,Obs, col, indexFunc,(col-1)*9 + 1:col*9);
end
indexFunc = @(row, col) row + 9*(col - 1);
for block1 = 1:3:9
    e1 = block1 + 2;
    for block2 = 1:3:9
        e2 = block2 + 2;
        [c,r] = meshgrid(block1:e1,block2:e2);
        indices = reshape(indexFunc(c,r).',[1 9]);
        Obs = countCandidateOccurences(C(indices));
        C = applyExclusiveValue(C,Obs, block1,indexFunc, indices);
    end
end

%% THERE IS A BUG IN THIS FUNCTION
function C = blockBlockInteraction(C)
% Consider blocks in turn
% if a number can only occur in a given row in a block, then this number can be eleminated from all other plces in the same
for b11 = 1:3:9
    for b12 = 1:3:9
        block1Cand = getBlockCandidates(b11,b12, C); % 3x3 cell
        for b21 = b11
            for b22 = b12 + 3:3:9
                block2Cand = getBlockCandidates(b21,b22, C); % 3x3 cell
                restBlockCol = setdiff([1 4 7], [b12 b22]);
                %restBlock = getBlockCandidates(b21,restBlockCol, C);
                disp(['(' num2str(b11) ',' num2str(b12) ') and (' num2str(b21) ',' num2str(b22) ') -> rest: (' num2str(b21) ',' num2str(restBlockCol)])
                % For each number, find the rows that has i as a candidate
                for i = 1:9
                    ri = [];
                    for row = 1:3
                        rowCand = [block1Cand{:,row} block2Cand{:,row}];
                        if any(rowCand == i)
                            ri = [ri row];
                        end
          
                    end
                    % If i can only be placed in some of the rows, then we
                    % can eliminate it from these rows in the "restBlock"
                    if length(ri) < 3
                        rows = (b11-1) + ri;
                        cols = restBlockCol:restBlockCol + 2;
                        C = eliminateValueFromBlock(C,rows,cols,i);
                    end
                end
            end
        end
        for b21 = b11 + 3: 3 : 9
            for b22 = b12
                restBlockRow = setdiff([1 4 7], [b11 b21]);
                block2Cand = getBlockCandidates(b21,b22, C); % 3x3 cell
                disp(['(' num2str(b11) ',' num2str(b12) ') and (' num2str(b21) ',' num2str(b22) ') -> rest: (' num2str(restBlockRow) ',' num2str(b12)])
                for i = 1:9
                    ci = [];
                    for col = 1:3
                        colCand = [block1Cand{col,:} block2Cand{col,:}];
                        if any(colCand == i)
                            ci = [ci row];
                        end
                        
                    end
                    % If i can only be placed in some of the rows, then we
                    % can eliminate it from these rows in the "restBlock"
                    if length(ci) < 3
                        rows = restBlockRow:restBlockRow+2;
                        cols = (b12-1) + ci;
                        C = eliminateValueFromBlock(C,rows,cols,i);
                    end
                end
            end
        end
    end
end


%% THERE IS A BUG IN THIS FUNCTION?
function C = blockDimInteraction(C)
% Consider blocks in turn
% if a number can only occur in a given row in a block, then this number can
% be eleminated from all other places in the same row, likewise for columns  
for dim = 1:2 % 1 = row, 2 = column
    for b1 = 1:3:9
        for b2 = 1:3:9
            blockCandidates = getBlockCandidates(b1,b2, C); % 3x3 cell
            for rowcol = 1:3
                start = (2*dim-1)*rowcol - 2*dim + 2;
                len = -4*dim + 10;
                indimIndex = linspace(start,start + len,3);
                inrowcol = [blockCandidates{indimIndex}]; % NB: This may cause problem in a future release
                restIndex = setdiff(1:9, indimIndex);
                rest = [blockCandidates{restIndex}];
                uniqueCandidates = setdiff(inrowcol, rest);
                if ~ isempty(uniqueCandidates)
                    C = eliminateValueFromRestOfDim(C, uniqueCandidates, b1, b2, rowcol, dim);
                end
            end
        end
    end
end

function C = nakedSubset(C)


function bcands = getBlockCandidates(b1, b2, C)
    C = reshape(C, 9, 9);
    bcands = C(b1:b1+2, b2:b2+2);
    
function C = eliminateValueFromBlock(C,rows, cols, values)
linind = getLinearIndex(rows,cols);
for l = linind
    C{l} = setdiff(C{l}, values);
end

function C = eliminateValueFromRestOfDim(C, uniqueCandidates, b1, b2, relrowcol, dim)
if dim == 1
d1 = b2;d2 = b1;
rows = d2 + relrowcol - 1;
cols = setdiff(1:9,d1:d1+2);
else
d1 = b1; d2 = b2;
rows = setdiff(1:9,d1:d1+2);
cols = d2 + relrowcol - 1;
end

linind = getLinearIndex(rows, cols);
for l = linind
   C{l} = setdiff(C{l}, uniqueCandidates); 
end

function C = eliminateValueFromRestOfRow(C, uniqueCandidates, b1, b2, relrow)
absrow = b1 + relrow -1;
cols = setdiff(1:9,b2:b2+2);
linind = getLinearIndex(absrow, cols);
for l = linind
   C{l} = setdiff(C{l}, uniqueCandidates); 
end

function C = eliminateValueFromRestOfCol(C, uniqueCandidates, b1, b2, relcol)
abscol = b2 + relcol -1;
rows = 1:9;

linind = getLinearIndex(rows, abscol);
for l = linind
   C{l} = setdiff(C{l}, uniqueCandidates); 
end


function ind = getLinearIndex(rows, cols)
% Turns (rows,columns) into linear index
ind = [];
for row = rows
    for col = cols
        ind(end +1) = row + 9*(col-1);
    end
end

        
        


    


function r = isFinishedSudoku(M)
r = all(all(M));



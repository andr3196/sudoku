classdef sudoku
    
    properties
        M
        sz
        P
    end
    
    methods
        function obj = sudoku(n)
            if nargin > 0
                if isnumeric(n)
                    if numel(n) == 81
                        obj.M = n;
                    else
                        obj.M = nan(n);
                    end
                end
            else
                x = 0;
                obj.M = [5 3 x x 7 x x x x;
                    6 x x 1 9 5 x x x;
                    x 9 8 x x x x 6 x;
                    8 x x x 6 x x x 3;
                    4 x x 8 x 3 x x 1;
                    7 x x x 2 x x x 6;
                    x 6 x x x x 2 8 x;
                    x x x 4 1 9 x x 5;
                    x x x x 8 x x 7 9];
                obj.M = [3   0   0   0   0   0   0   2   0
   0   0   0   6   0   0   0   0   4
   0   0   0   0   5   0   0   0   0
   7   0   0   0   0   0   0   5   0
   0   0   0   0   0   6   3   0   0
   0   0   0   2   0   0   0   0   0
   0   0   0   0   7   0   1   8   0
   0   4   6   0   0   0   9   0   0
   0   2   0   0   0   0   0   0   0];
               
            end
            obj.sz = numel(obj.M);
            obj.P = [];
        end
        
        function r = checkRow(obj,rowInd)
            %Get row
            R = obj.M(rowInd,:);
            R(R==0) = [];
            r = numel(R) == numel(unique(R));
        end
        
        function r = checkCol(obj,colInd)
            %Get col
            C = obj.M(:,colInd);
            C(C==0) = [];
            r = numel(C) == numel(unique(C));
        end
        
        function r = checkSquare(obj,row,col)
            ri = ceil(row/3);
            ci = ceil(col/3);
            rb = (ri - 1)*3+1;
            re = 3*ri;
            cb = (ci - 1)*3+1;
            ce = 3*ci;
            C = obj.M(rb:re,cb:ce);
            C(C==0) = [];
            r = numel(C) == numel(unique(C));
        end
        
        function r = hasEmptyPlacedList(obj)
            r = isempty(obj.P);
        end
        
        function appendPlacedList(obj,v)
            obj.P(end+1) = v;
        end
        
        function v = popPlacedList(obj)
            v = obj.P(end-1);
            obj.P(end) = [];
        end
        
        function v = peakPlacedList(obj)
            v = obj.P(end);
        end
        
        function printSudoku(obj)
            s = '';
            function s = appendRow(s, row)
                s = [s '|'];
                for j = 1:2*length(row)
                    if mod(j,2) == 1
                        s = [s ' '];
                    else
                        v = row(j/2);
                        if v == 0
                            s = [s ' '];
                        else
                            s = [s num2str(v)];
                        end
                    end
                    if mod(j,6) == 0
                        s = [s ' |'];
                    end
                end
                s = [s '\n'];
            end
            function s = makeSection(s,sec)
                s = [s '-------------------------\n'];
                for i = 3*(sec - 1)+1:3*sec
                    s = appendRow(s,obj.M(:,i));
                end
            end
            
            for k = 1:3
               s = makeSection(s,k); 
            end
            s = [s '-------------------------'];
            sprintf(s)
        end
        
        function sref = subsref(obj,s)
            switch s(1).type
                case '.'
                    %%% Should also handle methods without return values
                    switch s(1).subs
                        case 'M'
                            sref = obj.M;
                        case 'sz'
                            sref = obj.sz;
                        case 'hasEmptyPlacedList'
                            sref = hasEmptyPlacedList(obj);
                        case 'appendPlacedList'
                            appendPlacedList(obj,s(2).subs{1});
                        case 'checkCol'
                            sref = checkCol(obj,s(2).subs{1});
                        case 'checkRow'
                            sref = checkRow(obj,s(2).subs{1});
                        case 'checkSquare'
                            sref = checkSquare(obj,s(2).subs{1},s(2).subs{1,2});
                        case 'popPlacedList'
                            sref = popPlacedList(obj);
                        case 'peakPlacedList'
                            sref = peakPlacedList(obj);
                        otherwise
                            error(['Unknown attribute: ' s(1).subs]);
                    end
                    
                case '()'
                    if length(s)<2
                        sref = builtin('subsref',obj.M,s);
                        return
                    else
                        sref = builtin('subsref',obj,s);
                    end
                case '{}'
                    error('MyDataClass:subsref',...
                        'Not a supported subscripted reference')
            end
        end
        
        function r = isNoOutputMethod(method)
            switch method
                case {'appendPlacedList', 'printSudoku'}
                    r = true;
                otherwise
                    r = false;
            end
        
        end
        
        function obj = subsasgn(obj,s,val)
            if isempty(s) && isa(val,'MyDataClass')
                obj = sudoku;
            end
            switch s(1).type
                s(1).subs
                case '.'
                    obj = builtin('subsasgn',obj,s,val);
                case '()'
            
                    if length(s)<2
                        if isa(val,'sudoku')
                            error('sudoku:subsasgn',...
                                'Object must be scalar')
                        elseif isa(val,'double')
                            snew = substruct('.','M','()',s(1).subs(:));
                            obj = subsasgn(obj,snew,val);
                        end
                    end
                case '{}'
                    error('MyDataClass:subsasgn',...
                        'Not a supported subscripted assignment')
            end
        end
        
    end
    
    
end